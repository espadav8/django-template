#!/usr/bin/env bash

set -o errexit
set -o pipefail

CMD="$@"

function pg_isready(){
    while ! nc -z "${POSTGRES_HOST}" "${POSTGRES_PORT}"; do
      sleep 0.1
    done
}

until pg_isready; do
  >&2 echo "Postgres is unavailable - sleeping"
  sleep 1
done

>&2 echo "Postgres is up - continuing..."
exec ${CMD}
